# WindRose
This is the WindRose project, which allows the developer to create an orthogonal RPG game using a maps layout.

# Install
To install this package you need to open the package manager in your project and:

  1. Add a scoped registry with:
     - "name": "AlephVault"
     - "url": "https://unity.packages.alephvault.com"
     - "scopes": ["com.alephvault"]
  2. And another registry with:
     - "name": "GameMeanMachine"
     - "url": "https://unity.packages.gamemeanmachine.com"
     - "scopes": ["com.gamemeanmachine"]
  2. Look for this package: `com.gamemeanmachine.unity.windrose`.
  3. Install it.

# Notes
This documentation has to be updated after the big migration.